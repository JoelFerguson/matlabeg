function averageVal = computeAverage(inputMatrix)
    % Returns the average value of a matrix input
    
    % Determine size of matrix
    [m, n] = size(inputMatrix);
    % Sum over all elements
    sum = 0;
    for i=1:m
        for j=1:n
            sum = sum + inputMatrix(i,j);
        end
    end
    % Compute average value
    averageVal = sum/(i*j);
end