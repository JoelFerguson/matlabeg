clear
clc

% Generate matrix of ones
A = ones(10,5);
% Compute average value
[average, stdDev] = computeStdDev(A);

% Generate matrix of ones
B = zeros(7,8);
% Compute average value
[average2, stdDev2] = computeStdDev(B)
