clear
clc

% Generate matrix of ones
A = ones(20,20);
% Compute average value
average = computeAverage(A)

% Test random matrix
B = rand(4,4);
% Compute average value
average = computeAverage(B)
