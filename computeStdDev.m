function [mean, standardDev] = computeStdDev(inputMatrix)

    %compute mean
    mean = computeAverage(inputMatrix);
    
    % Compute numerator
    [m,n] = size(inputMatrix);
    sum = 0;
    for i=1:m
        for j=1:n
            sum = sum + (inputMatrix(i,j) - mean)^2;
        end
    end

    % Compute standard deviation
    standardDev = sqrt(sum/(m*n - 1))
end